import React from 'react';

import './Input.css';

const Input = ({ message, setMessage, sendMessage, sendHighFive }) => (
  <form className="form">
     <button className="sendHighFive" onClick={(event) => sendHighFive(event)}>High Five</button>

    <input className="input" type="text" placeholder="Type message ..."
      value={message}
      onChange={(event) => setMessage(event.target.value)}
      onKeyPress={(event) => event.key === 'Enter' ? sendMessage(event) : null}
    />

    <button className="sendButton" onClick={(event) => sendMessage(event)}>Send</button>

   
  </form>
)

export default Input;
