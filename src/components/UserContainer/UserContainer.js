import React, { useState } from 'react';
import onlineIcon from '../../images/onlineIcon.png';
import Select from 'react-select'

import './UserContainer.css';
import axios from 'axios';
const options = [
  { value: 1, label: '1 Hour' },
  { value: 2, label: '2 Hour' },
  { value: 3, label: '3 Hour' },
  { value: 4, label: '4 Hour' },
  { value: 5, label: '5 Hour' },
]

const UserContainer = ({ users }) => {
  const [stats, setStats] = useState([]);
  const onSelectChange = async (event) => {
    console.log('select Change Called')
    setStats([])
    const resp = await axios.get(`http://localhost:8000/filter-by-hours/${event.value}`)
    setStats(resp.data)
  }
  return (<div className="textContainer">
    {
      users
        ? (
          <div class="user-container">
            <h3>Online Users</h3>
            <div className="activeContainer">
              <p>
                {users.map(({ name }) => (
                  <div key={name} className="activeItem">
                    {name}
                    <img alt="Online Icon" src={onlineIcon} />
                  </div>
                ))}
              </p>
            </div>
            <div>
              <h4>Chat Stats</h4>
              <h5>View Chat Stats by :</h5>
              <Select className="basic-single"
                classNamePrefix="select" onChange={onSelectChange} options={options} />
            </div>
            <div style={{ 'overflow-y': 'scroll', overflow: 'auto', 'height': '300px' }}>
              <ul>
                {stats.map(stat => (
                  <li>{`${new Date(stat.timestamp).getHours()}:${new Date(stat.timestamp).getMinutes()}`}: {stat.type}</li>
                ))}
              </ul>
            </div>
          </div>
        )
        : null
    }
  </div>)
};

export default UserContainer;
